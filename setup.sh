#!/bin/bash

destdir="/opt/bind9"

#Make directory structure
mkdir -p $destdir/volumes/{etc,cache,lib,log}

#Move initial config files into the volumes
cp -r etc/* $destdir/volumes/
cp -r lib/* $destdir/volumes/
cp docker-compose.yml $destdir/

echo "Update /opt/bind9/volumes/etc/named.local with your domains then run docker-compose up -d."

